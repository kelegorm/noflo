library noflo.utils.names;

/// Uses last part of component to generate id.
String genNameByType(String nodeType, List<String> existed) {
  var shortName = nodeType.replaceFirst(new RegExp('.*/'), '');
  return genNextName(shortName, existed);
}


String genNextName(String possibleName, List<String> existed) {
  var digits = lastDigitsRe.firstMatch(possibleName)?.group(1) ?? '';
  var number = digits.isNotEmpty ? num.parse(digits) : 2;

  var shortName = possibleName.substring(0, possibleName.length - digits.length);
  var newLabel = digits.isNotEmpty ? '$shortName${number}' : shortName;

  while (existed.contains(newLabel)) {
    newLabel = '$shortName${number++}';
  }

  return newLabel;
}

List<String> genNextNames(List<String> names, List<String> existedNames) {
  var result = <String>[];
  var reservedNames = existedNames.toList();

  names.forEach((label) {
    var nextLabel = genNextName(label, reservedNames);

    reservedNames.add(nextLabel);
    result.add(nextLabel);
  });

  return result;
}


final RegExp lastDigitsRe = new RegExp(r'(\d+)$');
final RegExp beforeDigitsRe = new RegExp(r'^(.*)(\d*)$');