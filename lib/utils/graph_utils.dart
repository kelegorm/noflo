library noflo.utils.graph_utils;

import 'dart:math';
import 'package:noflo/src/classes.dart';
import 'package:noflo/src/graph.dart';

/// Clones [nodes] and [connections], makes new ids and adds into [graph].
///
/// Doesn't changes parameters.
///
/// Returns added nodes.
List<Node> pasteExt(Graph graph, List<Node> nodes, List<Connection> connections) {
  var oldIds = nodes.map<String>((n) => n.id);
  var newIds = new Map<String, String>.fromIterables(oldIds, graph.generateIds(oldIds));

  // clone nodes
  var newNodes = new List<Node>.unmodifiable(nodes.map<Node>((n) => n.clone(newId: newIds[n.id])));
  graph.addNodes(newNodes);

  var newConnections = new List<Connection>.unmodifiable(connections.map<Connection>((c) {
    if (c is SourceConnection) {
      return c.clone(
          newTarget: newIds[c.target.process] ?? c.target.process,
          newSource: newIds[c.source.process] ?? c.source.process);
    } else {
      return c.clone(
          newTarget: newIds[c.target.process] ?? c.target.process
      );
    }
  }));

  graph.addConnections(newConnections);

  return newNodes;
}


void shiftNodes(Iterable<Node> nodes, {int times}) {
  times ??= 1;

  if (times < 1) return;

  nodes.forEach((node) {
    node.metadata.x += Graph.horizontalDistanceBetweenLogicBlocks * times;
    node.metadata.y += Graph.verticalDistanceBetweenLogicBlocks * times;
  });
}


void placeAtFreeSpace(Graph graph, Iterable<Node> nodes, {bool keepStructure: false}) {
  var point = getFreeSpaceCoords(graph, ignore: nodes);
  placeNodesAt(point, nodes, keepStructure: keepStructure);
}


void placeNodesAt(Point<int> point, Iterable<Node> nodes, {bool keepStructure: false}) {
  if (keepStructure) {
    _placeAt(point, nodes);
  } else {
    _placeInRow(point, nodes);
  }
}

/// Returns position of free space, bellow nodes.
///
/// Can ignore some nodes, in case if you need to find position for that
/// nodes.
///
/// x coordinate will be the x coordinate of the most left block or zero.
/// y - lower than y coordinate of the most low located block.
Point<int> getFreeSpaceCoords(Graph graph, {Iterable<Node> ignore: const []}) {
  var firstBlock = true;
  var xCoord = 0;
  var yCoord = 0;
  Node lowestBlock;

  for (var node in graph.nodes) {
    if (ignore?.contains(node) ?? false) continue;

    num processX = node.metadata.x;
    num processY = node.metadata.y;

    if (firstBlock) {
      firstBlock = false;
      xCoord = processX;
      yCoord = processY;
      lowestBlock = node;
    } else {
      if (xCoord > processX) xCoord = processX;
      if (yCoord < processY) {
        yCoord = processY;
        lowestBlock = node;
      }
    }
  }

  if (!firstBlock) {
    yCoord += lowestBlock.metadata.height + Graph.verticalDistanceBetweenLogicBlocks;
  }
  return new Point(xCoord.toInt(), yCoord.toInt());
}


/// Places nodes at point, keeps relative positions.
void _placeAt(Point<int> point, List<Node> newNodes) {
  if (newNodes.isEmpty) return;

  var topPosition = newNodes.first.metadata.y;
  var leftPosition = newNodes.first.metadata.x;

  newNodes.skip(1).forEach((n) {
    if (n.metadata.y < topPosition) topPosition = n.metadata.y;
    if (n.metadata.x < leftPosition) leftPosition = n.metadata.x;
  });

  var shiftY = point.y - topPosition;
  var shiftX = point.x - leftPosition;

  newNodes.forEach((n) {
    n.metadata.y += shiftY;
    n.metadata.x += shiftX;
  });
}


/// Places [newNodes] in a row from [startPoint].
void _placeInRow(Point<int> startPoint, List<Node> newNodes) {
  var xPos = startPoint.x;

  for (var node in newNodes) {
    node.metadata.x = xPos;
    node.metadata.y = startPoint.y;
    xPos += node.metadata.width + Graph.horizontalDistanceBetweenLogicBlocks;
  }
}