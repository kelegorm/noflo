library noflo.graph;

import 'dart:math' show Point;

import 'package:noflo/src/classes.dart';
import 'package:noflo/utils/graph_utils.dart';
import 'package:noflo/utils/id_utils.dart' as idUtils;


class Graph {
  /// Mixed list of connections and initials.
  Iterable<Connection> get connections => _connections;

  final Map<String, Map> inports;

  Iterable<Node> get nodes => _nodes.values;

  /// Labels objects to make logic code more readable.
  final Map<String, Map> notes;

  final Map<String, Map> outports;


  /// Other fields in logic json:
  ///   "properties":{}, "groups":[]
  factory Graph.fromJson(Map json) {
    var nodes = <String, Node>{};
    var jsNodes = json['processes'];
    if (jsNodes is Map<String, Map>) {
      jsNodes.forEach((id, node) {
        nodes[id] = new Node.fromJson(id, node);
      });
    }

    var connections = new Set<Connection>();
    var jsCons = json['connections'];
    if (jsCons is List<Map>) {
      connections = jsCons.map<Connection>((map) {
        return new Connection.fromJson(map);
      }).toSet();
    }

    var jsInports = json['inports'];
    var inports = {};
    if (jsInports is Map) inports = jsInports;

    var jsOutports = json['outports'];
    var outports = {};
    if (jsOutports is Map) outports = jsOutports;

    var jsNotes = json['notes'];
    var notes = {};
    if (jsNotes is Map) notes = jsNotes;

    var graph = new Graph._(
        new Map.from(inports),
        new Map.from(outports),
        new Map.from(notes)
    );

    graph.addNodes(nodes.values);
    graph.addConnections(connections);

    return graph;
  }

  Graph._(this.inports, this.outports, this.notes) {
    if (_nodes == null) throw new ArgumentError('processes should not be null');
    if (connections == null) throw new ArgumentError('connections should not be null');
    if (inports == null) throw new ArgumentError('inports should not be null');
    if (outports == null) throw new ArgumentError('outports should not be null');
    if (notes == null) throw new ArgumentError('notes should not be null');
  }


  void addConnection(Connection connection, {bool override: true}) {
    if (connection is! DataConnection && connection is! SourceConnection) {
      throw new ArgumentError('Argument should be DataConnection or SourceConnection.');
    }

    if (_connections.contains(connection)) {
      throw new ArgumentError('Connection instance is alredy registered in the graph.');
    }

    if (!_nodes.containsKey(connection.target.process)) {
      throw new ArgumentError('Target id is invalid: ${connection.target.process}.');
    }

    if (connection is SourceConnection && !_nodes.containsKey(connection.source.process)) {
      throw new ArgumentError('Source id is invalid: ${connection.source.process}.');
    }

    var existedEqualConn = _connections.firstWhere((c) => _isEqualConnections(c, connection), orElse: () => null);

    if (existedEqualConn != null) {
      if (override) {
        _connections.remove(existedEqualConn);
      } else {
        throw new ArgumentError('Same connection is already registered.');
      }
    }

    _connections.add(connection);
  }

  void addConnections(Iterable<Connection> connections) {
    for (var c in connections) {
      addConnection(c);
    }
  }

  /// TODO check nodes ids to be good value and make tests.
  void addNode(Node node) {
    if (_nodes.containsValue(node)) throw new ArgumentError('Node instance already registered in graph.');
    if (_nodes.containsKey(node.id)) throw new ArgumentError('Node id is not unique.');
    if (node.id == null || node.id.isEmpty) throw new ArgumentError('Node id is not valid.');

    _nodes[node.id] = node;
  }

  void addNodes(Iterable<Node> newNodes) {
    for (var node in newNodes) {
      addNode(node);
    }
  }

  /// Adds [newNodes] and places it in one row.
  ///
  /// You can pass here already existed nodes(same id) using [skipExisted] flag.
  /// But ensure that nodes are very same instances.
  void addNodesAt(List<Node> newNodes, {Point<int> point, bool keepStructure: false, bool skipExisted: false}) {
    for (var node in newNodes) {
      if (skipExisted && _nodes.containsKey(node.id)) {
        if (!identical(_nodes[node.id], node)) {
          throw new ArgumentError('Node id is already registered for other Node instance: ${node.id}');
        }
        continue;
      }
      addNode(node);
    }

    point ??= getFreeSpaceCoords(this, ignore: newNodes);
    placeNodesAt(point, newNodes, keepStructure: keepStructure);
  }

  bool containsId(String processId) => _nodes.containsKey(processId);

  /// Generates and returns node.
  ///
  /// Doesn't make changes in logic itself.
  ///
  /// Used in tests only.
  Node generateNode(String id, String _type, {String label, String sublabel, Point<int> position}) {
    var uniqueLabel = (label != null) ? generateLabel(label) : id;
    var json = _generateNodeJson(_type, uniqueLabel, sublabel: sublabel);

    return new Node.fromJson(id, json);
  }

  /// Generates and returns node with new unique id.
  ///
  /// Generates unique id based on already existed nodes in logic.
  ///
  /// Doesn't make changes in logic itself.
  Node generateNodeAndId(String _type, {String label, String sublabel, Point<int> position}) {
    var id = _generateIdByType(_type);
    var uniqueLabel = (label != null) ? generateLabel(label) : id;
    var json = _generateNodeJson(_type, uniqueLabel, sublabel: sublabel);

    var node = new Node.fromJson(id, json);

    if (position != null) {
      node.metadata.y = position.y;
      node.metadata.x = position.x;
    }

    return node;
  }

  /// Generates unique process id by [nodeType].
  ///
  /// Gets last part of [nodeType] and add number to generate uniq id.
  String _generateIdByType(String nodeType) {
    var existedIds = _nodes.keys.toList();
    return idUtils.genNameByType(nodeType, existedIds);
  }

  List<String> generateIds(Iterable<String> ids) {
    var existedIds = _nodes.keys.toList();
    return idUtils.genNextNames(ids.toList(), existedIds);
  }

//  /// Generates few new ids like [genNameByType].
//  ///
//  /// [nodeTypes] is list of types for each node you need.
//  List<String> generateIdsByType(Iterable<String> nodeTypes) {
//    var shortNames = nodeTypes.map<String>((t) => t.replaceFirst(new RegExp('.*/'), '')).toList();
//    var existedIds = processes.keys.toList();
//    var result = <String>[];
//
//    shortNames.forEach((name) {
//      var newId = idUtils.genNameByType(name, existedIds);
//      result.add(newId);
//      existedIds.add(newId);
//    });
//
//    return result;
//  }


  String generateNoteId() {
    var i = 0;
    var name = "note";
    while (notes.containsKey("$name$i")) {
      i++;
    }
    return "$name$i";
  }

  /// Adds number to [label] if some process already has same one.
  ///
  /// It's not about generating node id!
  ///
  /// It's need just to distinguish widgets like "Button" and "Button".
  /// They would be better as "Button" and "Button2".
  ///
  /// it's node designed to get label with number.
  String generateLabel(String label) {
    var existedLabels = _nodes.values.map<String>((n) => n.metadata.label).toList();
    return idUtils.genNextName(label, existedLabels);
  }

  List<String> generateLabels(List<String> labels) {
    var existedLabels = _nodes.values.map<String>((n) => n.metadata.label).toList();
    return idUtils.genNextNames(labels, existedLabels);
  }

  Node getNode(String id) {
    if (!_nodes.containsKey(id)) throw new StateError('Graph doesn\'t contains node with id $id.');
    return _nodes[id];
  }

  List<Node> getNodes(Iterable<String> ids) => ids.map<Node>((id) => getNode(id)).toList();

  bool hasId(String id) => _nodes.containsKey(id);

  /// Checks if [node] has incoming data on [port].
  bool hasInputConnections(String node, {String port}) {
    return connections
        .any((con) => con.target.process == node
        && (port == null || port == con.target.port));
  }

  /// Removes node and all it's connections and initials.
  void removeNode(String id) {
    if (!_nodes.containsKey(id)) {
      throw new StateError('Grapg doesn\'t contain node with id "$id"');
    }

    _nodes.remove(id);
    _removeConnections(id);
    //todo remove graph's inports and outports to and from node.
  }

  /// Generates nofloGraph object for the-graph-editor.
  Map<String, Object> toJson() => {
    'processes' : new Map<String, Map>.fromIterables(_nodes.keys, _nodes.values.map((n) => n.toJson())),
    'connections' : connections.map<Map>((con) => con.toJson()).toList(),
    'inports' : new Map.from(inports),
    'outports' : new Map.from(outports),
    'notes' : new Map.from(notes),
    'properties' : {},
    'groups' : [],
  };


  /// Generates process node and returns it.
  ///
  /// Doesn't add it into processes.
  Map<String, Object> _generateNodeJson(String _type, String label, {String sublabel}) {
    var metadata = {
      "label": label,
      "sublabel": sublabel ?? _type,
      "width": logicBlockDefaultWidth,
      "height": logicBlockDefaultHeight,
      "x": 0,
      "y": 0,
    };

    return {
      "component": _type,
      "metadata": metadata
    };
  }

  /// Removes all connections for node.
  void _removeConnections(String id) {
    var toRemove = _connections.where((c) => c.target.process == id || (c is SourceConnection && c.source.process == id));
    _connections.removeAll(toRemove.toList());
  }


  final Map<String, Node> _nodes = <String, Node>{};
  final Set<Connection> _connections = new Set<Connection>();


  /// Checks if connections has same target and source.
  ///
  /// Ignores data for [DataConnection]s.
  static bool _isEqualConnections(Connection a, Connection b) {
    if (a is SourceConnection && b is SourceConnection) {
      return a.target == b.target && a.source == b.source;
    }

    if (a is DataConnection && b is DataConnection) {
      return a.target == b.target;
    }

    return false;
  }

  static final int logicBlockDefaultWidth = 72;
  static final int logicBlockDefaultHeight = 72;
  static final int horizontalDistanceBetweenLogicBlocks = 36;
  static final int verticalDistanceBetweenLogicBlocks = 36;
}