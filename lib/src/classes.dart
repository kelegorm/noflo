library noflo.classes;

import 'dart:math' show Rectangle;

import 'package:meta/meta.dart';
import 'package:quiver_hashcode/hashcode.dart';


class Node {
  /// Unique id of process.
  final String id;

  /// Type of node, process class.
  final String component;

  /// Node's size, position and caption.
  final NodeMetadata metadata;


  Node(this.id, this.component, this.metadata);

  factory Node.fromJson(String id, Map json) {
    if (id == null || id.isEmpty) throw new ArgumentError('Id must be not empty string.');

    var jsComponent = json["component"];
    String component;
    if (jsComponent is String) component = jsComponent;
    if (component == null || component.isEmpty) throw new ArgumentError('"component" field must be specified.');

    var jsMetadata = json["metadata"];
    Map metadata;
    if (jsMetadata is Map) metadata = jsMetadata;
    if (metadata == null || component.isEmpty) throw new ArgumentError('"metadata" field must be specified and not empty.');

    return new Node(id, component, new NodeMetadata.fromJson(metadata));
  }

  factory Node.fromNoflo(Map map) => new Node.fromJson(map['id'], map);

  
  Node clone({String newId}) => new Node(
      newId ?? id,
      component,
      metadata.clone(),
  );

  /// Returns json for logic map.
  Map<String, Object> toJson() => {
    "component": component,
    "metadata": metadata.toJson(),
  };

  Map<String, Object> toNoflo() {
    var res = toJson();
    res["id"] = id;
    return res;
  }

  String toString() => '$id, $component, ${metadata.toRectangle()}';
}


/// Node's caption, size and position.
///
/// It's only about position of node in graph. It's have nothing
/// with how logic works or where widget is placed.
class NodeMetadata {
  int get x => _x;
  void set x(int val) {
    if (val == null) throw new ArgumentError('x should not be null.');
    _x = val;
  }

  int get y => _y;
  void set y(int val) {
    if (val == null) throw new ArgumentError('y should not be null.');
    _y = val;
  }

  int get height => _height;
  void set height(int val) {
    if (val == null) throw new ArgumentError('height should not be null.');
    _height = val;
  }

  int get width => _width;
  void set width(int val) {
    if (val == null) throw new ArgumentError('width should not be null.');
    _width = val;
  }

  /// Visible name of label.
  String label;

  /// Little label below [label].
  ///
  /// For now it's a equal [component].
  String sublabel;


  NodeMetadata({int x, int y, int height, int width, this.label, this.sublabel}) {
    this.x = x;
    this.y = y;
    this.height = height;
    this.width = width;
  }

  factory NodeMetadata.fromJson(Map json) {
    return new NodeMetadata(
      x: json["x"],
      y: json["y"],
      height: json["height"],
      width: json["width"],
      label: json["label"],
      sublabel: json["sublabel"],
    );
  }


  /// Returns clone of this.
  ///
  /// Pass new [x], [y] and [label] if you want update it.
  NodeMetadata clone({int x, int y, String label}) => new NodeMetadata(
    x: x ?? this.x,
    y: y ?? this.y,
    label: label ?? this.label,
    height: height,
    width: width,
    sublabel: sublabel,
  );

  Map<String, Object> toJson() => {
    "x": x,
    "y": y,
    "height": height,
    "width": width,
    "label": label,
    "sublabel": sublabel,
  };

  Rectangle<int> toRectangle() => new Rectangle<int>(x, y, width, height);


  int _x;
  int _y;
  int _height;
  int _width;
}


/// Base class of connections in NoFlo graph.
///
/// It's Dart representation of connection objects.
@immutable
abstract class Connection {
  /// Connection's destination process and port.
  final Target target;


  factory Connection.fromJson(Map json) {
    if (json.containsKey('data')) return new DataConnection.fromJson(json);
    if (json.containsKey('src')) return new SourceConnection.fromJson(json);
    throw new ArgumentError('json is not correct');
  }

  Connection._(this.target);

  Map<String, Object> toJson();

  Connection clone({String newTarget});
}


class SourceConnection extends Connection {
  /// Connection's destination process and port.
  final Target source;


  SourceConnection({
    @required String sourceId,
    @required String sourcePort,
    @required String targetId,
    @required String targetPort, Map metadata
  }): this.source = new Target(sourceId, sourcePort),
        super._(new Target(targetId, targetPort));

  /// Creates connection based on json(map).
  factory SourceConnection.fromJson(Map map) => new SourceConnection(
      sourceId: map['src']['process'],
      sourcePort: map['src']['port'],
      targetId: map['tgt']['process'],
      targetPort: map['tgt']['port']
  );

  factory SourceConnection.fromNoflo(Map map) => new SourceConnection(
      sourceId: map['from']['node'],
      sourcePort: map['from']['port'],
      targetId: map['to']['node'],
      targetPort: map['to']['port']
  );


  SourceConnection clone({String newSource, String newTarget}) => new SourceConnection(
      sourceId: newSource ?? source.process,
      sourcePort: source.port,
      targetId: newTarget ?? target.process,
      targetPort: target.port
  );

  Map<String, Map<String,String>> toJson() {
    var json = <String, Map<String,String>>{
      "src": source.toJson(),
      "tgt": target.toJson()
    };

    return json;
  }

  /// Return map in noflo style.
  Map<String, Map<String,String>> toNoflo() => <String, Map<String,String>>{
    "from": source.toNoflo(),
    "metadata": {},
    "to": target.toNoflo()
  };

  String toString() => '{$source -> $target}';
}


class DataConnection extends Connection {
  final Object data;


  DataConnection({
    @required Object data,
    @required String targetId,
    @required String targetPort
  }): this.data = data,
        super._(new Target(targetId, targetPort));

  factory DataConnection.fromJson(Map init) => new DataConnection(
      data: init['data'],
      targetId: init['tgt']['process'],
      targetPort: init['tgt']['port']
  );

  factory DataConnection.fromNoflo(Map map) => new DataConnection(
      data: map['from']['data'],
      targetId: map['to']['node'],
      targetPort: map['to']['port']
  );


  DataConnection clone({String newTarget}) => new DataConnection(
      data: data,
      targetId: newTarget ?? target.process,
      targetPort: target.port
  );

  Map<String, Object> toJson() => <String, Object>{
    "data": data,
    "tgt": target.toJson()
  };

  String toString() => '{$data -> $target}';
}


@immutable
class Target {
  final String process;
  final String port;


  Target(this.process, this.port);


  @override
  int get hashCode => hash3(65, process, port);

  @override
  bool operator ==(Object other) => other is Target && other.process == this.process && other.port == this.port;

  Map<String, String> toJson() => {
    "process": process,
    "port": port
  };

  String toString() => '$process.$port';

  Map<String, String> toNoflo() => {
    'node': process,
    'port': port
  };
}