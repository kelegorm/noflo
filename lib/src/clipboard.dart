library noflo.clipboard;

import 'package:noflo/src/classes.dart';
import 'package:noflo/src/graph.dart';
import 'package:noflo/utils/graph_utils.dart' as utils;

class Clipboard {
  /// We get map from TheGraph.Clipboard. It makes new nodes ids by
  /// copy command, so we already have here new ids.
  Clipboard.fromJson(Map map) {
    var nodes = map['nodes'];
    if (nodes is List<Map>) {
      _nodes.addAll(nodes.map<Node>((m) => new Node.fromNoflo(m)));
    }

    var initials = map['initializers'];
    if (initials is List<Map>) {
      _initials.addAll(initials.map<DataConnection>((m) => new DataConnection.fromNoflo(m)));
    }

    var edges = map['edges'];
    if (edges is List<Map>) {
      _edges.addAll(edges.map<SourceConnection>((m) => new SourceConnection.fromNoflo(m)));
    }

    _times = map['pasted'] ?? 0;

    print(map['pasted']);
  }

  List<Node> paste(Graph graph) {
    var cons = new List<Connection>.from(_initials)..addAll(_edges);
    var newNodes = utils.pasteExt(graph, _nodes, cons);
    utils.shiftNodes(newNodes, times: _times + 1);
    return newNodes;
  }

  final List<Node> _nodes = <Node>[];
  final List<DataConnection> _initials = <DataConnection>[];
  final List<SourceConnection> _edges = <SourceConnection>[];
  int _times = 0;
}