import 'package:noflo/utils/id_utils.dart';
import "package:test/test.dart";


void main() {

  // Base static function to generate next free label or id.
  group('genNextName', () {
    test('one by one check', () {
      expect(genNextName('label',[]), equals('label'));
      expect(genNextName('label',['label']), equals('label2'));
      expect(genNextName('label1',['label1']), equals('label2'));
      expect(genNextName('label1',['label','label1']), equals('label2'));
      expect(genNextName('label',['label','label1']), equals('label2'));
      expect(genNextName('label',['label1','label2']), equals('label'));
      expect(genNextName('label2',[]), equals('label2'));
    });
  });


  // Same function as prev one, but works with lists.
  group('genNextNames', () {
    test('one by one check', () {
      expect(genNextNames(['label','label','label'],['label']), orderedEquals(['label2','label3','label4']));
      expect(genNextNames(['label','label','label'],['label','label1']), orderedEquals(['label2','label3','label4']));
      expect(genNextNames(['label1','label1','label1'],['label','label2','label4']), orderedEquals(['label1','label3','label5']));
    });

    test('doesn\'t modify input list', () {
      var existed = ['label','label1'];
      genNextNames(['label','label','label'],['label','label1']);
      expect(existed, orderedEquals(['label','label1']));
    });
  });


  // Same function but cuts last part after '/'.
  group('genNameByType', () {
    test('first ids', () {
      expect(genNameByType('types/SomeType', []), equals('SomeType'));
      expect(genNameByType('SomeOtherType', []), equals('SomeOtherType'));
    });

    test('second ids', () {
      var existed = ['SomeType','SomeOtherType'];
      expect(genNameByType('types/SomeType', existed), equals('SomeType2'));
      expect(genNameByType('SomeOtherType', existed), equals('SomeOtherType2'));
    });

    test('third ids', () {
      var existed = ['SomeType','SomeOtherType','SomeType1','SomeOtherType1'];
      expect(genNameByType('types/SomeType', existed), equals('SomeType2'));
      expect(genNameByType('SomeOtherType', existed), equals('SomeOtherType2'));
    });

    test('ids with gaps', () {
      var existed = ['SomeOtherType','SomeType1'];
      expect(genNameByType('types/SomeType', existed), equals('SomeType'));
      expect(genNameByType('SomeOtherType', existed), equals('SomeOtherType2'));
    });
  });
}