import "package:test/test.dart";

import 'graph_test_utils.dart';


void main() {
  group('Graph.generateUniqueLabel', () {
    test('Empty grid check', () {
      expect(gnl('label'), equals('label'));
      expect(gnl('label1'), equals('label1'));
      expect(gnl('label2'), isNot(equals('label3')));
    });

    // That label generation looks stupid.
    test('Add label and generate new one', () {
      expect(gnla('label'), equals('label2'));
      expect(gnla('label1'), equals('label2'));
      expect(gnla('label2'), equals('label3'));
      expect(gnla('label3'), equals('label4'));
    });
  });

  // Checking new node id generation, made by generateNodeAndId().
  // That method should reuse genNodeIdByType, so let's just check
  // all is right in one big check.
  group('Graph.generateNodeAndId', () {
    test('one massive check', () {
      var graph = createGraphWithNodes(ids: ['SomeType1','SomeOtherType','SomeOtherType3']);

      var types = ['types/SomeType', 'types/SomeType', 'types/SomeType', 'SomeOtherType', 'SomeOtherType', 'SomeOtherType'];
      var ids = fillGraphWithTypes(types, graph);

      expect(ids, orderedEquals([
        '${'SomeType'}',
        '${'SomeType'}2',
        '${'SomeType'}3',
        '${'SomeOtherType'}2',
        '${'SomeOtherType'}4',
        '${'SomeOtherType'}5',
      ]));
    });
  });

//  // Generation ids by generateNodeIds.
//  group('Graph.genIdsByTypes', () {
//    test('one massive check', () {
//      var graph = createGraphWithNodes(ids: ['SomeType2','SomeOtherType','SomeOtherType4']);
//
//      var nodeTypes = ['types/SomeType', 'types/SomeType', 'types/SomeType', 'SomeOtherType', 'SomeOtherType', 'SomeOtherType'];
//      var newIds = graph.generateIdsByType(nodeTypes);
//
//      expect(newIds, orderedEquals([
//        '${'SomeType'}',
//        '${'SomeType'}3',
//        '${'SomeType'}4',
//        '${'SomeOtherType'}2',
//        '${'SomeOtherType'}3',
//        '${'SomeOtherType'}5',
//      ]));
//    });
//  });
}