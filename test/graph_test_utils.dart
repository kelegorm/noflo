import 'package:meta/meta.dart';
import 'package:noflo/noflo.dart';

Graph emptyGraph() => new Graph.fromJson({});

/// Returns new label.
///
/// Generates label in empty graph.
String gnl(String label) {
  var graph = emptyGraph();
  return graph.generateLabel(label);
}

/// Returns new label.
///
/// Firstly adds initial label into graph.
String gnla(String label) {
  var graph = emptyGraph();
  var node = graph.generateNode('notMatterId', 'NotMatterType', label: label);
  graph.addNodesAt([node]);
  return graph.generateLabel(label);
}


Graph createGraphWithNodes({@required List<String> ids}) {
  var graph = emptyGraph();

  fillGraphWithIds(ids, graph);

  return graph;
}

void fillGraphWithIds(List<String> ids, Graph graph) {
  ids.forEach((id) {
    var node = graph.generateNode(id, 'NotMatterType');
    graph.addNodesAt([node]);
  });
}

List<String> fillGraphWithTypes(List<String> types, Graph graph) {
  var ids = <String>[];

  types.forEach((c) {
    var node = graph.generateNodeAndId(c);
    ids.add(node.id);
    graph.addNodesAt([node]);
  });

  return ids;
}